<?php
/*
Plugin Name: Orange Template Addon
Description: The customized styling used in the default Orange theme, deactivate and delete me before your journey
Version: 1.0
Author: OrangeDigital
*/


/**
 * Enqueue OTA styles.
 *
 * @since 1.0.0
 *
 * @internal
 */
function ota_add_styles() {
	wp_enqueue_style( 'ota-css', plugin_dir_url(__FILE__ ) .  "template-style.css");
}
add_action( 'wp_enqueue_scripts', 'ota_add_styles' );
