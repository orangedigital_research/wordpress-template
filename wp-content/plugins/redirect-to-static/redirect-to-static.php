<?php
   /*
   Plugin Name: Redirect to Static
   Plugin URI: 
   description: Sync Redirection list to Simply Static
   Version: 1.0
   Author: Orange Digital
   Author URI: 
   License: GPL2
   */
?>
<?php
function redirect_to_static_init() {

    if($_SERVER["REQUEST_URI"] == '/redirect-sitemap.xml') {
        $pluginList = get_option( 'active_plugins' );
        $redirectionCheck = 'redirection/redirection.php';
        if ( !in_array( $redirectionCheck , $pluginList ) ) {
            return;
        }

        // Generate a sitemap that will be crawled
        global $wpdb;

        // Get the list of redirect urls
        $redirectUrls = $wpdb->get_results(
            $wpdb->prepare("SELECT {$wpdb->prefix}redirection_items.url FROM {$wpdb->prefix}redirection_items
            WHERE {$wpdb->prefix}redirection_items.status='%s'", 'enabled')
        );

        /*
        Alvin idea
        Generate a sitemap with all the redirects in it
        */

        if (is_array($redirectUrls) && !empty($redirectUrls)) {
            // $file = ABSPATH.'redirect-sitemap.xml';
            $baseURL = get_site_url();

            $xml = '<?xml version="1.0" encoding="UTF-8"?>'."\n";
            $xml.= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n";

            // Loop through all links
            foreach ($redirectUrls as $url) {
                $xml.= '<url>'."\n";
                $xml.= '<loc>'.$baseURL.$url->url.'</loc>'."\n";
                $xml.= '<lastmod>'.date('c').'</lastmod>'."\n";
                $xml.= '</url>'."\n";
            }

            $xml.= '</urlset>';

            // file_put_contents($file, $xml);

            header('Content-type: text/xml');
            echo $xml;
            exit();
        }
    }
}

// Should change this so it doesn't fire all the time
// add_action( 'init', 'redirect_to_static_init' );
// add_action('post_updated', 'redirect_to_static_init');
add_action('parse_request', 'redirect_to_static_init');
?>