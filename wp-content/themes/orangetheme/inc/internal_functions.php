<?php 
/*************************** Useful functions ********************************/

/*
 * Input a string, return the string with restricted length and "..."
 *
 * Example:
 * String: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et"
 * Length: 5
 * Output: "Lorem..."
 */

function restrict_string_length($target_string, $length){
	$string = strip_tags($target_string);
	if (strlen($string) > $length) {

	    // truncate string
	    $stringCut = substr($string, 0, $length);
	    $endPoint = strrpos($stringCut, ' ');

	    //if the string doesn't contain any space then it will cut without word basis.
	    $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
	    $string .= '...';
	}
	return $string;
}

/*
 * HEX to RGBA
 */
function hex_rgba($hex, $opacity = '1'){
    list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");
    return "rgba(" . $r . ", " . $g . ", " . $b . ", " . $opacity . ")";
}


/*
 * RGB to HEX
 */
function rgba_hex($r, $g, $b) {
    $hex = sprintf("#%02x%02x%02x", $r, $g, $b);
    return $hex;
}


/*
 * Get image alt from URL, if alt is empty, will take image name as alt
 */
function image_alt_by_url( $image_url ) {
    global $wpdb;

    if( empty( $image_url ) ) {
        return false;
    }

    $image = $wpdb->get_results("SELECT ID, post_title FROM `wp_posts` WHERE `guid` LIKE '%" . trim($image_url) . "%'");
    $image_id = trim($image['0']->ID);
    $image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', true);
    
    if(trim($image_alt) == ''){
        $image_alt = $image['0']->post_title;
    }

    return $image_alt;
}

?>