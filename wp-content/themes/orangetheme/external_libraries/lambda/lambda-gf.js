jQuery(function($){
	gravityformHook();
});


/*
Gravity Forms
*/
function gravityformHook(){
    if ($('.blk_form .gform_wrapper form').length>0) {
		$('.blk_form').find('.form-messages').prependTo('.blk_form .gform_footer');

        $('.blk_form .gform_wrapper form input,.blk_form .gform_wrapper form textarea, .blk_form .gform_wrapper form select').on('focus',function(event){
            $(this).removeClass('invalid');
            $(this).parents('.gfield').find('.field-message').remove();
        });

        $('.blk_form .gform_wrapper form input[type=submit]').on('click',function(event){
        	event.preventDefault();
        	//Profanity Start
        	let all_input = "";
        	let target_form = $(this).parents('.blk_form');
        	let target_submit = this;
        	target_form.find('.gform_body textarea').each(function() {
        		all_input += $(this).val();;
        	});

			if(all_input !== ''){
				const settings = {
					"async": true,
					"crossDomain": true,
					"url": "https://community-purgomalum.p.rapidapi.com/containsprofanity?text=" + all_input,
					"method": "GET",
					"headers": {
						"X-RapidAPI-Host": "community-purgomalum.p.rapidapi.com",
						"X-RapidAPI-Key": "7c54f0b7e6msh7f43a79eeb4c02ep14b0dbjsn0ba247452dcd"
					}
				};

				$.ajax(settings).done(function (response) {
					if(response == "true") {
						if(target_form.find('.form-messages .error-message').length > 0) {
							//if found replace the message
							target_form.find('.form-messages .error-message').html('*Words with profanity detected, please review the field above');
						} else {
							//replacing profanity_warning_msg into error-message class
							target_form.find('.form-messages').html('<p class="error-message">*Words with profanity detected, please review the field above</p>');
						}
						return;
					}else{
						//no cursing word found, procce to next step!
						if(target_form.find('.form-messages .error-message').length > 0) {
							target_form.find('.form-messages .error-message').remove();
						}
						if(target_form.find('.field-message').length > 0) {
							target_form.find('.field-message').remove();
						}

						submit_form(target_submit);
					}
				});
				//Profanity End
			} else {
				submit_form(target_submit);
			}
			
		});
		
    }
}


function submit_form(target) {
	var form_validation = true;
		$(target).parents('form').find('input[aria-required="true"]').each(function(){
			if($(this).val() == ''){
				$(this).addClass('invalid');
				form_validation = false;
			}else{
				$(this).removeClass('invalid');
			}
		})
		$(target).parents('form').find('select[aria-required="true"]').each(function(){
			if($(this).val() == ''){
				$(this).addClass('invalid');
				form_validation = false;
			}else{
				$(this).removeClass('invalid');
			}
		})
		if(!form_validation){
			//$(this).find('.gform_button').attr('disabled','disabled');
			$(target).parents('.blk_form').find('.form-messages').html('<div class="error-message">There was a problem with your submission. Please review the fields above.</div>');

		}else{
			$(target).parents('.blk_form').find('.form-messages').html('');
	        const gravityformURL = $(target).parents('.blk_form').attr('data-gf-url').replace('=','');
	        const gravityformID = $(target).parents('.blk_form').attr('data-gf-id');
	        const gravityformBaseURL = $(target).parents('.blk_form').attr('data-gf-baseurl').replace('=','');

	        //console.log('Caught form');
	        //console.log({gravityformURL});
	        
	        // Get all data
	        const output = {};
	        const values = {};
	        $.each($(target).parents('form').serializeArray(), function(i, field) {
	            if (field.name.startsWith('input_')) {
	                values[field.name] = field.value;
	            }
	        });
	        output.baseUrl = gravityformBaseURL;
	        output.payload = values;
	        //console.log({output});

	        const json = JSON.stringify(output);
	        //console.log({json});

	        var currentForm = $(target).parents('form');

	        $.ajax({
	    		method:'POST',
	            url: gravityformURL,
	            data: json,
	            contentType: 'application/json',
	            beforeSend: function(){
	                // Show spinner
	                $(currentForm).find('.gform_button').attr('disabled','disabled');
	                //$(currentForm).parents('.blk_form').addClass('loading');
	                $(currentForm).parents('.blk_form').find('.form-messages').html('');
	                //$(currentForm).parents('.blk_form').addClass('form-loading');
	                //$(currentForm).parents('.blk_form').find('input[type="submit"]').addClass('loading');
	                $(currentForm).parents('.blk_form').find('input[type="submit"]').val('Submitting...');
	            },
	            success: function(data){
	                /*
	                {"is_valid":true,"page_number":0,"source_page_number":1,"confirmation_message":"<div id='gform_confirmation_wrapper_1' class='gform_confirmation_wrapper '><div id='gform_confirmation_message_1' class='gform_confirmation_message_1 gform_confirmation_message'>Thanks for contacting us! We will get in touch with you shortly.<\/div><\/div>","confirmation_type":"message"}

	                {"is_valid":false,"validation_messages":{"1":"This field is required.","3":"This field is required.","4":"This field is required."},"page_number":1,"source_page_number":1}

	                {"is_valid":false,"validation_messages":{"5":"This field is required."},"page_number":1,"source_page_number":1}
	                */
	                //console.log({data});
	                const result = data;
	                // return;
	                // const result = JSON.parse(data);
	                //console.log({result});

	                // sucessful
	                if (result.status == 'success') {
	                    if (result.confirmation_message) {
	                        $(currentForm).parents('.blk_form').find('.form-messages').html(result.confirmation_message);
	                        $(currentForm)[0].reset();
	                        $(currentForm).find('.gform_button').removeAttr('disabled');
	                        //$(currentForm).parents('.blk_form').removeClass('form-loading');
	                        //$(currentForm).parents('.blk_form').find('input[type="submit"]').removeClass('loading');
	                        $(currentForm).parents('.blk_form').find('field-message').remove();
	                        $(currentForm).parents('.blk_form').find('input[type="submit"]').val('Submit');
	                        setTimeout(function(){
	                            $(currentForm).parents('.blk_form').find('.form-messages').html('');
	                        },30000);

	                        window.dataLayer = window.dataLayer || [];
	                        window.dataLayer.push({
	                            event: "formSubmission",
	                            formID: gravityformID
	                        });

	                        return;
	                    }
	                }

	                // Do for errors
	                if (result.validation_messages) {
	                    $.each(result.validation_messages,function(i,e){
	                        //console.log({i,e});
	                        const currentField = $(currentForm).find('#field_'+gravityformID+'_'+i);
	                        $(currentField).find('input').addClass('invalid');
	                        //console.log($(currentField).find('input'));
	                        if($(currentField).find('.error-message-text.field-message').length == 0) {
	                 			$(currentField).append('<div class="field-message error-message-text">'+e+'</div>');
	                        } else {
	                        	//we replace the error message with the new one
	                        	$(currentField).find('.error-message-text.field-message').html(e);
	                        }

	                    });

	                    $(currentForm).find('.gform_button').removeAttr('disabled');
	                	$(currentForm).parents('.blk_form').find('input[type="submit"]').removeClass('loading');
	                    $(currentForm).parents('.blk_form').removeClass('form-loading');
	                }

	            },
	            error: function(xhr,status,error){
	                //console.log({xhr,status,error});
	                $(currentForm).parents('.blk_form').find('.form-messages').html('<div class="error-message">There was an error sending message. Please try again later.</div>');
	                $(currentForm).find('.gform_button').removeAttr('disabled');
	                $(currentForm).parents('.blk_form').removeClass('form-loading');
	                $(currentForm).parents('.blk_form').find('input[type="submit"]').removeClass('loading');

	                setTimeout(function(){
	                    $(currentForm).parents('.blk_form').find('.form-messages').html('');
	                },30000);
	            }
	        });
		}

}