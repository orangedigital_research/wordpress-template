<?php
/**
 * Template Name: Page (Default)
 *
 */

get_header();

?>

<div class="mainContent">
    <?php 
        if(have_rows('block')):
            while (have_rows('block')): the_row();
                echo get_template_part('blocks/' . get_row_layout() );
            endwhile;
        endif;
    ?>
</div>

<?php get_footer(); ?>


