<?php
/**
 * WP Search With Algolia instantsearch template file.
 *
 * @author  WebDevStudios <contact@webdevstudios.com>
 * @since   1.0.0
 *
 * @version 2.0.0
 * @package WebDevStudios\WPSWA
 */

get_header();

?>
	<div class="algolia-search-wrapper">
		<div class="container mt-5 mb-5">
			<div class="algolia-search-box-wrapper">
				<div id="search-box"></div>
				<div id="searchbox">

				</div>
			</div>
			<div id="hits"></div>
			<div id="pagination"></div>
			<div id="stats"></div>
		</div>
	</div>


	<script type="text/html" id="tmpl-search-submit">
		submit
	</script>


	<script type="text/html" id="tmpl-instantsearch-hit">
		<article itemtype="http://schema.org/Article">
			<# if ( data.images.thumbnail ) { #>
				<div class="thumbnail">
					<a href="{{ data.permalink }}" title="{{ data.post_title }}" class="thumbnail-link">
						<img src="{{ data.images.thumbnail.url }}" alt="{{ data.post_title }}" title="{{ data.post_title }}" itemprop="image" />
					</a>
				</div>
			<# } #>

			<div class="content">
				<h2 itemprop="name headline"><a href="{{ data.permalink }}" title="{{ data.post_title }}" class="title-link" itemprop="url">{{{ data._highlightResult.post_title.value }}}</a></h2>
				<div class="excerpt">
					<p>
						<# if ( data._snippetResult['content'] ) { #>
							<span class="suggestion-post-content content-snippet">{{{ data._snippetResult['content'].value }}}</span>
						<# } #>
					</p>
				</div>
			</div>
			<div class="ais-clearfix"></div>
		</article>
	</script>


	<script type="text/javascript">
		jQuery(function() {
			if(jQuery('#search-box').length > 0) {

				if (algolia.indices.searchable_posts === undefined && jQuery('.admin-bar').length > 0) {
					alert('It looks like you haven\'t indexed the searchable posts index. Please head to the Indexing page of the Algolia Search plugin and index it.');
				}

				/* Instantiate instantsearch.js */
				var search = instantsearch({
					indexName: algolia.indices.searchable_posts.name,
					searchClient: algoliasearch( algolia.application_id, algolia.search_api_key ),
					routing: {
						router: instantsearch.routers.history({ writeDelay: 1000 }),
						stateMapping: {
							stateToRoute( indexUiState ) {
								return {
									s: indexUiState[ algolia.indices.searchable_posts.name ].query,
									page: indexUiState[ algolia.indices.searchable_posts.name ].page
								}
							},
							routeToState( routeState ) {
								const indexUiState = {};
								indexUiState[ algolia.indices.searchable_posts.name ] = {
									query: routeState.s,
									page: routeState.page
								};
								return indexUiState;
							}
						}
					}
				});

				//custom searchbox --------
				const renderSearchBox = (renderOptions, isFirstRender) => {
					// Rendering logic
					const {query,refine, clear,isSearchStalled, widgetParams} = renderOptions;
					const container = document.querySelector('#searchbox');

					if (isFirstRender) {
						const form = document.createElement('form');
						form.addEventListener('submit', event => {
							event.preventDefault();
							refine(input.value);
						});

						const formGroup = document.createElement('div');
						formGroup.classList.add('row');

						const formCol1 = document.createElement('div');
						formCol1.classList.add('col');
						const formCol2 = document.createElement('div');
						formCol2.classList.add('col');

						const input = document.createElement('input');
						input.classList.add('form-control');

						const submit = document.createElement('button');
						submit.classList.add('btn','btn-primary');
						submit.type = 'submit';
						submit.appendChild(document.createTextNode('submit'));

						formCol1.appendChild(input);
						formCol2.appendChild(submit);
						formGroup.appendChild(formCol1);
						formGroup.appendChild(formCol2);
						form.appendChild(formGroup);
						container.appendChild(form);
					}

					container.querySelector('input').value = query;				
				};

				// 2. Create the custom widget
				const customSearchBox = instantsearch.connectors.connectSearchBox(
				renderSearchBox
				);

				// 3. Instantiate
				search.addWidgets([
					customSearchBox({
						// instance params
					})
				]);
				//end of customsearchbox ----------


				search.addWidgets([
					// /* Search box widget */
					// instantsearch.widgets.searchBox({
					// 	container: '#search-box',
					// 	placeholder: 'Search for...',
					// 	showReset: false,
					// 	showSubmit: true,
					// 	showLoadingIndicator: false,
					// 	cssClasses: {
					// 		root: 'searchForm-root',
					// 		form: [
					// 			'MyCusSearch',
					// 			'MyCusSearch--sub'
					// 		]

					// 	},
					// 	templates: {
					// 		submit: 'submit'
					// 	}
					// }),

					/* Stats widget */
					instantsearch.widgets.stats({
						container: '#stats',
						cssClasses: {
							root: 'MyCustomStats',
							text: ['MyCustomStatsText', 'MyCustomStatsText--subclass'],
						},
						templates: {
							text: `
							{{#areHitsSorted}}
								{{#hasNoSortedResults}}No relevant results{{/hasNoSortedResults}}
								{{#hasOneSortedResults}}1 relevant result{{/hasOneSortedResults}}
								{{#hasManySortedResults}}{{#helpers.formatNumber}}{{nbSortedHits}}{{/helpers.formatNumber}} relevant results{{/hasManySortedResults}}
								sorted out of {{#helpers.formatNumber}}{{nbHits}}{{/helpers.formatNumber}}
							{{/areHitsSorted}}
							{{^areHitsSorted}}
								{{#hasNoResults}}No results{{/hasNoResults}}
								{{#hasOneResult}}1 result{{/hasOneResult}}
								{{#hasManyResults}}{{#helpers.formatNumber}}{{nbHits}}{{/helpers.formatNumber}} results{{/hasManyResults}}
							{{/areHitsSorted}}
							found in {{processingTimeMS}}ms
							`,
						}
					}),

					/* Hits widget */
					instantsearch.widgets.hits({
						container: '#hits',
						hitsPerPage: 10,
						templates: {
							empty: 'No results were found for "<strong>{{query}}</strong>".',
							item: wp.template('instantsearch-hit')
						},
						transformData: {
							item: function (hit) {

								function replace_highlights_recursive (item) {
									if (item instanceof Object && item.hasOwnProperty('value')) {
										item.value = _.escape(item.value);
										item.value = item.value.replace(/__ais-highlight__/g, '<em>').replace(/__\/ais-highlight__/g, '</em>');
									} else {
										for (var key in item) {
											item[key] = replace_highlights_recursive(item[key]);
										}
									}
									return item;
								}

								hit._highlightResult = replace_highlights_recursive(hit._highlightResult);
								hit._snippetResult = replace_highlights_recursive(hit._snippetResult);

								return hit;
							}
						}
					}),

					/* Pagination widget */
					instantsearch.widgets.pagination({
						container: '#pagination'
					}),

				]);

				/* Start */
				search.start();

				jQuery( '#search-box input' ).attr( 'type', 'search' ).trigger( 'select' );
			}
		});
	</script>

<?php

get_footer();
