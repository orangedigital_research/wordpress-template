<?php 
    $media_source = get_sub_field('media_source');
    $youtube_video_id = get_sub_field('youtube_video_id');
    $vimeo_video_id = get_sub_field('vimeo_video_id');
    $video_path = get_sub_field('video_path');
    $video_poster = get_sub_field('video_poster');

    // Global settings
    $anchor = get_sub_field('settings')['anchor'];
    $bg_color = get_sub_field('settings')['background_color'];
    $padding_top = get_sub_field('settings')['padding_top'];
    $padding_bottom = get_sub_field('settings')['padding_bottom'];
?>

<div class="media_block <?php echo $bg_color . ' ' . $padding_top . ' ' . $padding_bottom; ?>">
	<?php if($anchor !== ''){ ?><div class="orangetheme-anchor"><a name="<?php echo $anchor; ?>"></a></div><?php } ?>
    <div class="container">
    	<div class="row justify-content-center align-items-center">
    		<div class="col-md-7">
    			<?php if($media_source == 'youtube'){ ?>
					<div id="plyr-media" data-type="youtube" data-video-id="<?php echo $youtube_video_id; ?>"></div>
    			<?php }else if($media_source == 'vimeo'){ ?>
					<div id="plyr-media" data-type="vimeo" data-video-id="<?php echo $vimeo_video_id; ?>"></div>
    			<?php }else if($media_source == 'video'){ ?>
					<video id="plyr-media" poster="<?php echo $video_poster; ?>" controls>
					  	<source src="<?php echo $video_path; ?>" type="video/mp4">
					</video>
    			<?php } ?>
    		</div>
    	</div>
    </div>
</div>


<script src="<?php echo get_stylesheet_directory_uri(); ?>/external_libraries/plyr/plyr.min.js"></script>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/external_libraries/plyr/plyr.min.css">

<script type="text/javascript">
	plyr.setup("#plyr-media");
</script>