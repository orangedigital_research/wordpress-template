<?php 
	$formID = get_sub_field('form');
    $formSubmitURL = str_replace('https://','https://=',get_field('gf_endpoint_url','options'));
    $formBaseURL = str_replace('https://','https://=',get_site_url(null,'/wp-json/gf/v2/forms/' . $formID));$form = get_sub_field("form");

    // Global settings
    $anchor = get_sub_field('settings')['anchor'];
    $bg_color = get_sub_field('settings')['background_color'];
    $padding_top = get_sub_field('settings')['padding_top'];
    $padding_bottom = get_sub_field('settings')['padding_bottom'];
?>

<div class="form_block <?php echo $bg_color . ' ' . $padding_top . ' ' . $padding_bottom; ?>">
	<?php if($anchor !== ''){ ?><div class="orangetheme-anchor"><a name="<?php echo $anchor; ?>"></a></div><?php } ?>
    <div class="container">
    	<div class="form_div blk_form" data-gf-url="<?php echo $formSubmitURL;?>" data-gf-id="<?php echo $formID;?>" data-gf-baseurl="<?php echo $formBaseURL;?>">
            <div class="form-messages"></div>
            <?php 
                gravity_form($formID, $display_title = false, $display_description = false);
            ?>
        </div>
    </div>
</div>