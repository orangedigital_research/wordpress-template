<?php 
    $title = get_sub_field("title");
    $text = get_sub_field("content");
    $cta_button = get_sub_field('cta_button');
    $image = get_sub_field('image');

    // Global settings
    $anchor = get_sub_field('settings')['anchor'];
    $bg_color = get_sub_field('settings')['background_color'];
    $padding_top = get_sub_field('settings')['padding_top'];
    $padding_bottom = get_sub_field('settings')['padding_bottom'];
?>

<div class="example_block <?php echo $bg_color . ' ' . $padding_top . ' ' . $padding_bottom; ?>">
	<?php if($anchor !== ''){ ?><div class="orangetheme-anchor"><a name="<?php echo $anchor; ?>"></a></div><?php } ?>
    <div class="container">
    	<div class="row justify-content-center align-items-center">
    		<div class="col-md-7">
    			<div class="content">
			    	<h2 class="mbottom20"><?php echo $title; ?></h1>	

			    	<?php if($text): ?>
			    		<div class="text mbottom20"><?php echo $text; ?></div>
			    	<?php endif; ?>

					<?php if($cta_button['button_label'] !== ''){ ?>
			            <a href= "<?php echo $cta_button['button_url']; ?>" class="btn <?php echo $cta_button['button_styling']?>"  <?php if($cta_button['open_in_new_tab']) { echo "target=_blank"; } ?>><?php echo $cta_button['button_label'] ?></a>
			        <?php } ?>
			    </div>
    		</div>
    		<div class="col-md-5">
    			<?php if(isset($image['url'])){ ?>
		            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="w-100">
		        <?php } ?>
    		</div>
    	</div>
    </div>
</div>