<?php 
    $carousel = get_sub_field("carousel");
   
    // Global settings
    $anchor = get_sub_field('settings')['anchor'];
    $bg_color = get_sub_field('settings')['background_color'];
    $padding_top = get_sub_field('settings')['padding_top'];
    $padding_bottom = get_sub_field('settings')['padding_bottom'];
?>

<div class="carousel_block <?php echo $bg_color . ' ' . $padding_top . ' ' . $padding_bottom; ?>">
	<?php if($anchor !== ''){ ?><div class="orangetheme-anchor"><a name="<?php echo $anchor; ?>"></a></div><?php } ?>
    <div class="container">
    	<div class="swiper-container">
			<div class="swiper-wrapper">
				<?php 
					foreach ($carousel as $key => $item) {
						$image = $item['image']['url'];
				?>
					<div class="swiper-slide" style="background-image: url(<?php echo $image;?>);"></div>
					<div class="swiper-slide" style="background-image: url(<?php echo $image;?>);"></div>
				<?php } ?>
			</div>
			<div class="swiper-pagination"></div>
			<div class="swiper-button-prev"></div>
			<div class="swiper-button-next"></div>
			<div class="swiper-scrollbar"></div>
		</div>
    </div>
</div>


<script src="<?php echo get_stylesheet_directory_uri(); ?>/external_libraries/swiper/swiper-bundle.min.js"></script>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/external_libraries/swiper/swiper-bundle.min.css">

<script type="text/javascript">
	var swiper = new Swiper('.swiper-container',{
	    // auto init the intance
        init: true,

        // 'horizontal' or 'vertical' 
        direction: 'horizontal',

        // target element to listen touch events on. 
        touchEventsTarget: 'container',

        // initial slide index
        initialSlide: 0,

        // animation speed
        speed: 300,

        // whether to use modern CSS <a href="https://www.jqueryscript.net/tags.php?/Scroll/">Scroll</a> Snap API.
        cssMode: false,

        // auto up<a href="https://www.jqueryscript.net/time-clock/">date</a> on window resize
        updateOnWindowResize: true,

        // Overrides
        width: null,
        height: null,

        // allow to change slides by swiping or navigation/pagination buttons during transition
        preventInteractionOnTransition: false,

        // for ssr
        userAgent: null,
        url: null,

        // To support iOS's swipe-to-go-back gesture (when being used in-app).
        edgeSwipeDetection: false,
        edgeSwipeThreshold: 20,

        // Free mode
        // If true then slides will not have fixed positions
        freeMode: false,
        freeModeMomentum: true,
        freeModeMomentumRatio: 1,
        freeModeMomentumBounce: true,
        freeModeMomentumBounceRatio: 1,
        freeModeMomentumVelocityRatio: 1,
        freeModeSticky: false,
        freeModeMinimumVelocity: 0.02,

        // Autoheight
        autoHeight: false,

        // Set wrapper width
        setWrapperSize: false,

        // Virtual Translate
        virtualTranslate: false,

        // slide' or 'fade' or 'cube' or 'coverflow' or 'flip'
        effect: 'slide',

        // Breakpoints
        breakpoints: {
            // when window width is >= 320px
            320: {
              slidesPerView: 2,
              spaceBetween: 20
            },
            // when window width is >= 480px
            480: {
              slidesPerView: 3,
              spaceBetween: 30
            },
            // when window width is >= 640px
            640: {
              slidesPerView: 4,
              spaceBetween: 40
            }
        },

    	pagination: {
            el: '.swiper-pagination',
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        scrollbar: {
            el: '.swiper-scrollbar',
        }
	});

</script>