<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

	<?php 
		$header_script = get_field('header_script', 'options'); 
		$header_script = str_replace('<p>', '', $header_script);
		$header_script = str_replace('</p>', '', $header_script);
		echo $header_script;
	?>

	<?php wp_head(); ?>
</head>


<body <?php body_class(); ?>>
<div id="wrapper">
	<header>
		<nav id="header" class="navbar navbar-expand-md <?php echo esc_attr( $navbar_scheme ); if ( isset( $navbar_position ) && 'fixed_top' === $navbar_position ) : echo ' fixed-top'; elseif ( isset( $navbar_position ) && 'fixed_bottom' === $navbar_position ) : echo ' fixed-bottom'; endif; if ( is_home() || is_front_page() ) : echo ' home'; endif; ?>">
			<div class="container">
				<a class="navbar-brand" href="<?php echo esc_url( home_url() ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
					<?php
						$header_logo = get_field( 'brand_logo', 'options');
						if ( !empty( $header_logo ) ) :
					?>
						<img src="<?php echo $header_logo['url']; ?>" alt="<?php echo $header_logo['alert_title']; ?>" />
					<?php
						endif;
					?>
				</a>

				<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="<?php esc_attr_e( 'Toggle navigation', 'orangetheme' ); ?>">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div id="navbar" class="collapse navbar-collapse">
					<?php
						// Loading WordPress Custom Menu (theme_location).
						wp_nav_menu(
							array(
								'theme_location' => 'main-menu',
								'container'      => '',
								'menu_class'     => 'navbar-nav me-auto',
								'fallback_cb'    => 'WP_Bootstrap_Navwalker::fallback',
								'walker'         => new WP_Bootstrap_Navwalker(),
							)
						);

					?>
						<form class="search-form my-2 my-lg-0" role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
							<div class="input-group">
								<input type="text" name="s" class="form-control" placeholder="<?php esc_attr_e( 'Search', 'orangetheme' ); ?>" title="<?php esc_attr_e( 'Search', 'orangetheme' ); ?>" />
								<button type="submit" name="submit" class="btn btn-outline-secondary"><?php esc_html_e( 'Search', 'orangetheme' ); ?></button>
							</div>
						</form>
				</div>
			</div>
		</nav>
			
	</header>

	<main id="main">
		
