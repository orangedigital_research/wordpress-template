<?php
/**
 * The Template for displaying all single posts.
 */

// $id = $post->ID;
// $title = $post->post_title;
// $featured_image = wp_get_attachment_url( get_post_thumbnail_id($id) );
// $excerpt = $post->post_excerpt;
// $content = $post->post_content;

get_header();

?>

<div class="mainContent">
    <?php 
        if(have_rows('block')):
            while (have_rows('block')): the_row();
                echo get_template_part('blocks/' . get_row_layout() );
            endwhile;
        endif;
    ?>
</div>

<?php get_footer(); ?>
