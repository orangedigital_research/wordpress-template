<?php
/**
 * Template Name: Not found
 * Description: Page template 404 Not found.
 *
 */

get_header();

?>

<div id="post-0" class="content error404 not-found container ptop100 pbottom100 text-center">
	<h1 class="entry-title"><?php esc_html_e( 'Not found', 'orangetheme' ); ?></h1>
	<div class="entry-content">
		<p><?php esc_html_e( 'It looks like nothing was found at this location.', 'orangetheme' ); ?></p>
	</div>
</div>

<?php get_footer();
