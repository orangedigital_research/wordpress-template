			</div>
		</main>
		<footer id="footer">
			<div class="container ptop80 pbottom80">
				<div class="row">
					<div class="col-md-3">
						<?php
							$header_logo = get_field( 'brand_logo', 'options');
							if ( !empty( $header_logo ) ) :
						?>
							<img class="footer_logo" src="<?php echo $header_logo['url']; ?>" alt="<?php echo $header_logo['alert_title']; ?>" />
						<?php
							endif;
						?>
					</div>
					<div class="col-md-3">
						<p><?php echo wp_get_nav_menu_name('footer-menu'); ?></p>
						<?php wp_nav_menu(
								array(
									'theme_location' => 'footer-menu',
									'container'      => '',
									'menu_class'     => 'navbar-nav me-auto',
								)
							);
						?>
					</div>
					<div class="col-md-3">
						<p><?php echo wp_get_nav_menu_name('footer-menu-2'); ?></p>
						<?php wp_nav_menu(
								array(
									'theme_location' => 'footer-menu-2',
									'container'      => '',
									'menu_class'     => 'navbar-nav me-auto',
								)
							);
						?>
					</div>
					<div class="col-md-3">
						<p><?php echo wp_get_nav_menu_name('footer-menu-3'); ?></p>
						<?php wp_nav_menu(
								array(
									'theme_location' => 'footer-menu-3',
									'container'      => '',
									'menu_class'     => 'navbar-nav me-auto',
								)
							);
						?>
					</div>
				</div>
			</div>
			<div class="container ptop30 pbottom30">
				<p class="copyright text-center font-14"><?php echo esc_html__( '&copy;') . " " . date('Y') . " " . get_field('copyright', 'options'); ?></p>
			</div>
		</footer>
	</div>
	<?php
		wp_footer();
	?>
</body>
<?php 
	$footer_script = get_field('footer_script', 'options'); 
	$footer_script = str_replace('<p>', '', $footer_script);
	$footer_script = str_replace('</p>', '', $footer_script);
	echo $footer_script;
?>
</html>
